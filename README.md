# iKaaS Demo Presentation
---
## Demo Dashboards
1. Open in a browser the Demo Dashboards which are available at (refer to the corresponding file): `http://<IP>:8700/dashboard`
2. Select the preferred deployment.
3. (Optionally open the Global Resource Catalogue UI)

## AAL Application
### Login Screen

1. Initially press **Settings** and configure the Global Service Manager IP in the option field **Global Service Manager address** in the following format:
`http://IP:PORT` (refer to the corresponding file). For example:
```
http://127.0.0.1:9090
```
2. Go back to the login screen and either sign up or login with your registered user.

### Smart Home & Wearables local clouds

1. Press the **Fetch available services** button in order to connect with the resource catalogue and fetch the available services for each local cloud.
2. Select the desired services and press the button **Create Service**.
3. Open the Demo Dashboards and select the **Global Service Manager** option. If you have selected any service from the **Wearables** local cloud, open the Demo Dashboards and select the **Timeseries Forecasting Vital Signs - Athens** option.
4. Press on the top of the application the *ikaas* logo, in order to open the sidebar navigation options.
5. Select the **Complex Service** option.
6. You can activate or deactivate the *Actuators*, press on the *Graph* button to fetch the current sensor data (this may take a while), navigate through the available tabs, see the notifications, if any.
7. Go back and press again the *ikaas* logo, to open the navigation sidebar options.
8. Select the option **Adjustments**.
9. Set the desired temperature and/or luminosity and press *Set*.
10. You can open again the **Complex Service** screen to see the status of the actuators changed accordingly.
11. Also, you can open the corresponding UI on the browser by the Demo Dashboards' option **Bayesian Statistics (Smart Home Automation) - Athens**.
12. By going back to the main screen or through the Home Automation notification you will see a dialog box, informing that home automation has been applied to the smart home and asks for the user's opinion about this adjustment (invoking the reinforcement learning technique).

### Smart City local cloud
1. Open the **Global Service Manager** UI.
2. The next scenario requires the user to change location from the smart home to the smart city domain. To achieve this simulation, press the button on the right top of the application (the blue cycle) and press the option **Change location** (we can see in the Global Service Manager UI the activation of the city cloud).
3. Open the sidebar navigation options and select the **Complex Service**.
4. You can press on the *Graph* button to fetch the current sensor data (this may take a while).
5. Open the sidebar navigation options and select the **Smart Mobility**.
6. Type the origin name and the destination and press the **Find Route** button.
7. If the optimal route has retrieved successfully press the **Open Map** button.
8. Open the Demo Dashboards and select the **Data Processing - Optimal Route Decision Making** option, so as to see the corresponding UI that explains the optimal route decision making algorithm.

### Crossborder Scenarios
1. The crossborder scenarios require the user to change location from Greece to Madrid or Tago-nishi. To achieve this simulation, press the button on the right top of the application (the blue cycle) and press the option **Settings**. In the settings screen select the option **User Location - Coordinates**. The valid options to write are {Madrid, Japan, Tagonishi}. In any other case, the default is Athens.
2. Open the **Complex Service** screen and you will see the corresponding city dashboard.
3. To see the dashboards in a greater screen than smart phone's, open the Demo Dashboards and select among the available **City Dashboard** options.
4. In Madrid case, you can also open the **Smart Mobility** screen to find the optimal route in Madrid, exploiting the Madrid's Data Processing component.
5. To show the timeseries forecasting for Tago-nishi open the Demo Dashboards and select the option **Timeseries Forecasting Vital Signs - Tagonishi** (when loads, select the second user).

### Logout
1. Open the **Global Service Manager** UI.
2. To logout from the application, press the button on the right top of the application (the blue cycle) and press **Logout** (highly recommended instead of Exit).
3. The user should disappear from the Global Service Manager UI.
